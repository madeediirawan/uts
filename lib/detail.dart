import 'package:flutter/material.dart';

final List<String> images = [
  'https://www.masakapahariini.com/wp-content/uploads/2019/11/shutterstock_1187855428-780x440.jpg',//0
  'https://cdn1-production-images-kly.akamaized.net/scjrybSFLjhU3c2kqVRXQpt0XWE=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2355321/original/081440500_1536562685-KEPITING_ASAM_MANIS_SANTAP_JOGJA_DOT_COM.jpg',
  'https://cdn0-production-images-kly.akamaized.net/ThQNW0HwBUAKex2kwFF6-OMlyOw=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2355322/original/076982800_1536562736-kepiting_saus_tiram_unilever_food_solutions.jpg',
  'https://cdn1-production-images-kly.akamaized.net/WdI_1iuKTh9By0ZdZ2r1AQjmFOg=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2355324/original/044342300_1536562831-kepiting_rebus_FOOD_NETWORK.jpeg',
  'https://cdn0-production-images-kly.akamaized.net/OrOHwrzJtd8kuTctxiyO6UahhEg=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2355325/original/020234000_1536562869-kepiting_asparagus_YELP.jpg',
  'https://cdn0-production-images-kly.akamaized.net/Loc7su7ob9YlrTqgIsvrdbjL_Yc=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2355327/original/039772100_1536562903-kepiting_lada_hitam.jpg',
  'https://cdn1-production-images-kly.akamaized.net/MM-xa_VOwuNhsV-33mL_M4GshD8=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2355330/original/027469600_1536562982-malaysias_most_wanted_food.jpg',
  'https://cdn1-production-images-kly.akamaized.net/42LGZ5wBgUwBrxwii59AJB9FF8Y=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2355331/original/048026900_1536563020-kare_kepiting_Jawa_Timur.jpg',
  'https://cdn0-production-images-kly.akamaized.net/-kE79BCxGabMzsOj0kqShriWQ-0=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2355332/original/002138100_1536563068-kepiting_soka_goreng_THE_SAUCY_SOUTHERNER.jpg',
];

class Kepiting extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    String image = images[1];
    return Scaffold(
      appBar: AppBar(
        title: Text('Resep Kepiting Asam Manis'),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.share), onPressed: (){},)
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Stack(
            children: <Widget>[
              Container(
                  height: 300,
                  width: double.infinity,
                  child: PNetworkImage(image,fit: BoxFit.cover,)),
              Container(
                margin: EdgeInsets.fromLTRB(16.0, 250.0,16.0,16.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5.0)
                ),
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Resep Kepiting Asam Manis", style: Theme.of(context).textTheme.title,),
                    SizedBox(height: 10.0),
                    Text("16 April 2021 By DLohani"),
                    SizedBox(height: 10.0),
                    Divider(),
                    SizedBox(height: 10.0,),
                    Row(children: <Widget>[
                      Icon(Icons.favorite_border),
                      SizedBox(width: 5.0,),
                      Text("20.2k"),
                      SizedBox(width: 16.0,),
                      Icon(Icons.comment),
                      SizedBox(width: 5.0,),
                      Text("2.2k"),
                    ],),
                    SizedBox(height: 10.0),
                    Text("Resep kepiting asam manis bisa jadi ide tepat untuk akhir pekan. Potongan kepiting segar dalam kuah yang terasa asam, manis, dan pedas. Terasa menyegarkan dengan tambahan air jeruk nipis. Sajikan di meja makan, dijamin kamu akan ketagihan."),
                    SizedBox(height: 10.0),
                    Text("Punya kepiting segar di rumah? Kita kreasikan dengan bumbu kaya rasa, yuk, dengan bumbu sederhana. Mulai dari cabai yang dihaluskan, kecap manis yang legit, hingga asam segar dari jeruk nipis. Semua bumbu ditumis hingga harum, kemudian masukkan ke dalam rebusan kepiting. Gunakan api kecil saat memasak agar bumbu cepat menyerap. Setelah kepiting matang, masukkan air perasan jeruk nipis sesaat sebelum api dimatikan."),
                    SizedBox(height: 10.0),
                    Text("<h3>Bahan:</h3><ul><li>3 Ekor kepiting sedang yang masih hidup</li> <li>6 Butir bawang merah</li> <li>6 Siung bawang putih</li> <li>2 cm jahe</li> <li>1 Sendok makan saus tiram</li><li>1 Sendok makan saus sambal/tomat</li><li>1 Sendok makan kecap manis</li> <li>1 Batang daun bawang</li><li>1 Sendok makan merica bubuk</li> <li>Garam secukupnya</li><li>Gula pasir secukupnya</li> <li>Minyak goreng secukupnya</li> </ul>"),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class PNetworkImage extends StatelessWidget {
  final String image;
  final BoxFit fit;
  final double width,height;
  const PNetworkImage(this.image, {Key key,this.fit,this.height,this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.network(
      image,
      // placeholder: (context, url) => Center(child: CircularProgressIndicator()),
      // errorWidget: (context, url, error) => Image.asset('assets/placeholder.jpg',fit: BoxFit.cover,),
      fit: fit,
      width: width,
      height: height,
    );
  }
}