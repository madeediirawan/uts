import 'package:ediuts/sidebar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'sidebar.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final List<String> images = [
    'https://cdn1-production-images-kly.akamaized.net/scjrybSFLjhU3c2kqVRXQpt0XWE=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2355321/original/081440500_1536562685-KEPITING_ASAM_MANIS_SANTAP_JOGJA_DOT_COM.jpg',
    'https://cdn0-production-images-kly.akamaized.net/ThQNW0HwBUAKex2kwFF6-OMlyOw=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2355322/original/076982800_1536562736-kepiting_saus_tiram_unilever_food_solutions.jpg',
    'https://cdn1-production-images-kly.akamaized.net/WdI_1iuKTh9By0ZdZ2r1AQjmFOg=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2355324/original/044342300_1536562831-kepiting_rebus_FOOD_NETWORK.jpeg',
    'https://cdn0-production-images-kly.akamaized.net/OrOHwrzJtd8kuTctxiyO6UahhEg=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2355325/original/020234000_1536562869-kepiting_asparagus_YELP.jpg',
    'https://cdn0-production-images-kly.akamaized.net/Loc7su7ob9YlrTqgIsvrdbjL_Yc=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2355327/original/039772100_1536562903-kepiting_lada_hitam.jpg',
    'https://cdn1-production-images-kly.akamaized.net/MM-xa_VOwuNhsV-33mL_M4GshD8=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2355330/original/027469600_1536562982-malaysias_most_wanted_food.jpg',
    'https://cdn1-production-images-kly.akamaized.net/42LGZ5wBgUwBrxwii59AJB9FF8Y=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2355331/original/048026900_1536563020-kare_kepiting_Jawa_Timur.jpg',
    'https://cdn0-production-images-kly.akamaized.net/-kE79BCxGabMzsOj0kqShriWQ-0=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2355332/original/002138100_1536563068-kepiting_soka_goreng_THE_SAUCY_SOUTHERNER.jpg',
  ];
  final List<String> nama =[
    'Kepiting Asam Manis',
    'Kepiting Saus Tiram Ala Chinese Food',
    'Kepiting Rebus',
    'Sup Kepiting Asparagus',
    'Kepiting Lada Hitam',
    'Kepiting Saus Padang',
    'Kepiting Telur Asin',
    'Kare Kepiting',
  ];

  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home :Scaffold(
        key: _key,
        drawer: Sidebar(),
        body: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
                expandedHeight: 150.0,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                  title: Text('Masakan'),
                  background: _buildSlider(),
                ),
                actions: <Widget>[
                  IconButton(
                    icon: const Icon(Icons.favorite_border),
                    tooltip: 'Favorites',
                    onPressed: () {/* ... */},
                  ),
                ]),
            SliverToBoxAdapter(
              child: Container(
                  color: Colors.blue,
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        MaterialButton(
                            onPressed: () {},
                            child: Text("New Update".toUpperCase(),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold))),
                        MaterialButton(
                            onPressed: () {},
                            child: Text("See All".toUpperCase(),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400))),
                      ],
                    ),
                  )),
            ),
            SliverPadding(
              padding: EdgeInsets.only(left: 16.0, right: 16.0),
              sliver: SliverGrid(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    mainAxisSpacing: 10.0,
                    crossAxisSpacing: 10.0,
                    childAspectRatio: 1.0,
                    crossAxisCount: 2),
                delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                    return _buildItems(index, context);
                  },
                  childCount: images.length,
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: Container(
                  margin: EdgeInsets.only(top: 20.0),
                  color: Colors.blue,
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        MaterialButton(
                            onPressed: () {},
                            child: Text("Featured".toUpperCase(),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold))),
                        MaterialButton(
                            onPressed: () {},
                            child: Text("See All".toUpperCase(),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400))),
                      ],
                    ),
                  )),
            ),
            SliverToBoxAdapter(
              child: _buildSlider(),
            ),
            SliverToBoxAdapter(
              child: Container(
                  padding: EdgeInsets.all(20.0),
                  color: Colors.blue,
                  child: Text("Recommended for you".toUpperCase(),
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold))),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                  return _buildListItem(index);
                },
                childCount: images.length,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildSlider() {
    return Container(
      padding: EdgeInsets.only(bottom: 20.0),
      height: 200.0,
      child: Container(
        child: Swiper(
          autoplay: true,
          itemBuilder: (BuildContext context, int index) {
            return new PNetworkImage(
              images[index],
              fit: BoxFit.cover,
            );
          },
          itemCount: 4,
          pagination: new SwiperPagination(),
        ),
      ),
    );
  }

  Widget _buildItems(int index, BuildContext context) {
    return Container(
      height: 200,
      child: GestureDetector(
        onTap: () => _onTapItem(context, index),
        child: Column(
          children: <Widget>[
            Expanded(
                child: Hero(
                    tag: "item$index",
                    child: PNetworkImage(images[index % images.length],
                        fit: BoxFit.cover))),
            SizedBox(
              height: 10.0,
            ),
            Text(nama[index],
              style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildListItem(int index) {
    return Container(
        height: 100,
        child: Card(
          child: Center(
            child: ListTile(
              leading: CircleAvatar(
                radius: 40,
                backgroundImage: NetworkImage(images[index % images.length]),
              ),
              title: Text(nama[index],
                softWrap: true,
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            ),
          ),
        ));
  }

  _onTapItem(BuildContext pcontext, int index) {
    Navigator.of(pcontext)
        .push(MaterialPageRoute<void>(builder: (BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: const Text("Masakan"),
        ),
        body: Material(
          child: Container(
            // The blue background emphasizes that it's a new route.
            color: Colors.white,
            padding: const EdgeInsets.all(16.0),
            alignment: Alignment.topLeft,
            child: GestureDetector(
              onTap: () => Navigator.pop(context),
              child: Column(
                children: <Widget>[
                  Expanded(
                      child: Hero(
                          tag: "item$index",
                          child: PNetworkImage(images[index % images.length],
                              fit: BoxFit.cover))),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(nama[index],
                    style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    }));
  }
}

class PNetworkImage extends StatelessWidget {
  final String image;
  final BoxFit fit;
  final double width,height;
  const PNetworkImage(this.image, {Key key,this.fit,this.height,this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.network(
      image,
      // placeholder: (context, url) => Center(child: CircularProgressIndicator()),
      // errorWidget: (context, url, error) => Image.asset('assets/placeholder.jpg',fit: BoxFit.cover,),
      fit: fit,
      width: width,
      height: height,
    );
  }
}