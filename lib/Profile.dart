import 'package:flutter/material.dart';

class MyProfil extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "Profil",
      home: new Scaffold(
        backgroundColor: Colors.white60,
        appBar: new AppBar(
            backgroundColor: Colors.redAccent,
            title: new Center(
              child: new Text("Profil"),
            )),
        body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 150,
                  width: 150,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      image: DecorationImage(
                          image: NetworkImage('https://scontent.fdps4-1.fna.fbcdn.net/v/t1.6435-9/95154772_1240878019577216_8758430840536432640_n.jpg?_nc_cat=103&ccb=1-3&_nc_sid=09cbfe&_nc_eui2=AeGGcksVIw7ST3BBEgiQop1nDsUH0RI4Ms0OxQfREjgyzW5kAa4ctzdT9VDCaDHPBXLbAP3Z_3bkq1u2xp9SRcBM&_nc_ohc=v5wrAU0eiQ4AX-0aH8Z&_nc_ht=scontent.fdps4-1.fna&oh=1cf7d9d40286210aff27c1a3b36148b6&oe=609E1A9B'), fit: BoxFit.cover)),
                ),
                Text(
                  "Made Edi Irawan",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    height: 2.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "1915051066",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15.0,
                    height: 1.0,
                  ),
                ),
                Card(
                  margin: EdgeInsets.only(top: 40.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Card(
                            color: Colors.lightGreenAccent,
                            margin: EdgeInsets.only(left: 10.0, right: 10.0),
                            child: Column(
                              children: <Widget>[
                                Icon(
                                  Icons.my_location,
                                  size: 110,
                                  color: Colors.black,
                                ),
                                Text(
                                  'Singaraja',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 17.0,
                                    height: 2.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            )),
                      ),
                      Expanded(
                        child: Card(
                            color: Colors.deepOrangeAccent,
                            margin: EdgeInsets.only(
                              left: 10.0,
                              right: 10.0,
                              top: 10.0,
                              bottom: 10.0,
                            ),
                            child: Column(
                              children: <Widget>[
                                Icon(
                                  Icons.home,
                                  size: 110,
                                  color: Colors.black,
                                ),
                                Text(
                                  'Gerokgak',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 17.0,
                                    height: 2.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            )),
                      )
                    ],
                  ),
                ),
                Card(
                  margin: EdgeInsets.only(top: 10.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Card(
                            color: Colors.yellow,
                            margin: EdgeInsets.only(left: 10.0, right: 10.0),
                            child: Column(
                              children: <Widget>[
                                Icon(
                                  Icons.music_note,
                                  size: 110,
                                  color: Colors.black,
                                ),
                                Text(
                                  'POP',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 17.0,
                                    height: 2.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            )),
                      ),
                      Expanded(
                        child: Card(
                            color: Colors.lightBlueAccent,
                            margin: EdgeInsets.only(
                              left: 10.0,
                              right: 10.0,
                              top: 10.0,
                              bottom: 10.0,
                            ),
                            child: Column(
                              children: <Widget>[
                                Icon(
                                  Icons.location_city,
                                  size: 110,
                                  color: Colors.black,
                                ),
                                Text(
                                  'Undiksha',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 17.0,
                                    height: 2.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            )),
                      )
                    ],
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}