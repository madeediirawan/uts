import 'package:flutter/material.dart';
import 'package:ediuts/form';
import 'Profile.dart';
class Sidebar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Remove padding
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            accountName: Text('Made Edi Irawan'),
            accountEmail: Text('made.edi@undiksha.ac.id'),
            currentAccountPicture: CircleAvatar(
              child: ClipOval(
                child: Image.network(
                  'https://scontent.fdps4-1.fna.fbcdn.net/v/t1.6435-9/95154772_1240878019577216_8758430840536432640_n.jpg?_nc_cat=103&ccb=1-3&_nc_sid=09cbfe&_nc_eui2=AeGGcksVIw7ST3BBEgiQop1nDsUH0RI4Ms0OxQfREjgyzW5kAa4ctzdT9VDCaDHPBXLbAP3Z_3bkq1u2xp9SRcBM&_nc_ohc=v5wrAU0eiQ4AX-0aH8Z&_nc_ht=scontent.fdps4-1.fna&oh=1cf7d9d40286210aff27c1a3b36148b6&oe=609E1A9B',
                  fit: BoxFit.cover,
                  width: 90,
                  height: 90,
                ),
              ),
            ),
            decoration: BoxDecoration(
              color: Colors.blue,
              image: DecorationImage(
                  fit: BoxFit.fill,
                  image: NetworkImage(
                      'https://scontent.fdps4-1.fna.fbcdn.net/v/t1.6435-9/67744737_119928912650034_1576921651025543168_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=8bfeb9&_nc_eui2=AeFn63ID72U5x5WFls2X4V8m74S_u12teiDvhL-7Xa16IAGsLXR6gkhPHKJ0gfD_8MzcfWn8fJVANHZB292we53F&_nc_ohc=MjIAKdUxWX8AX-oQJVk&_nc_ht=scontent.fdps4-1.fna&oh=f952b6cdd71c958162cf5ae3a8518648&oe=609D9ABF')),
            ),
          ),
          ListTile(
            leading: Icon(Icons.person),
            title: Text('My Profile'),
            onTap: () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => MyProfil())),
          ),
          ListTile(
            leading: Icon(Icons.book),
            title: Text('Bookmark'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.save),
            title: Text('Disimpan'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.notifications),
            title: Text('Notification & offers'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.cloud_upload),
            title: Text('Upload Masakan'),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => Forms()));
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text('Settings'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.description),
            title: Text('Policies'),
            onTap: () => null,
          ),
          Divider(),
          ListTile(
            title: Text('Logout'),
            leading: Icon(Icons.exit_to_app),
            onTap: () => null,
          ),
        ],
      ),
    );
  }
}